
 function tableStuff(page)
 {
   // removes final column from some tables - unnecessary
   if ((page === 'rh') || (page === 'holds'))
   {
     var c = document.querySelector('.patFuncHeaders:last-of-type');
     if (c)
     {
       c.style.display = 'none';
     }
     var pStatus = document.querySelectorAll('.patFuncStatus');
     if (pStatus) {
      for (var p = 0; p < pStatus.length; p++) {
        var q = pStatus[p];
        if (q.innerHTML.indexOf('Ready') > -1) {
        q.style.fontWeight = 'bold';
      }
        
      }
      
     }
     var d = document.querySelectorAll('.patFuncDetails');
     for (var i = 0; i < d.length; i++)
     {
       d[i].style.display = 'none';
     }
     var e = document.getElementsByTagName('td');
     for (var j = 0; j < e.length; j++)
     {
       e[j].removeAttribute('width');
     }
   }
   else if (page === 'items')
   {
     var f = document.querySelector('.patFuncHeaders');
     if (f)
     {
       f.querySelector('th.patFuncHeaders:nth-of-type(3)').style.display = 'none';
       var g = document.querySelectorAll('.patFuncBarcode');
       for (h = 0; h < g.length; h++)
       {
         g[h].style.display = 'none';
       }
     }
   }
 }
 function addClass(str) {
  var className = 'expanded';
  var el = document.getElementById(str);
  if (el.classList)
  el.classList.add(className);
else
  el.className += ' ' + className;
}
 function addGoogleEvent(el, cat, label) {
  el.addEventListener('click', function() {
    ga('send', 'event', cat, 'click', label);
    
    
    });
  
 }
 (function ()
 { // hide "about reading history" stuff on every page except the reading history page.
   var a = window.location.href;
   var nav = document.getElementById('nav');
   //     console.log(a);
   if (a.indexOf('overdues') > -1)
   {
    document.querySelector('#lrFinesAct a').setAttribute('class', 'current');
     var payButtons = document.querySelectorAll('.patFunc a');
     for (var k = 0; k < payButtons.length; k++) {
      payButtons[k].setAttribute('class', 'lr-button');
      payButtons[k].innerHTML = 'Pay Now';
     }
     addClass('lr-about-fines');
     
     var payOl = document.getElementsByClassName('lrPay');
     for (var ol = 0; ol < payOl.length; ol++) {
      addGoogleEvent(payOl[ol].parentNode, 'ecommerce', 'pay online');
     }
   }
   else if (a.indexOf('readinghistory') > -1)
   {
    document.querySelector('#lrCircHistAct a').setAttribute('class', 'current');
     var rh = 'lr-about-rh';
     tableStuff('rh');
     var itemLink = document.querySelectorAll('.patFuncEntry a');
     if (itemLink.length > 0)
     {
       for (var i = 0; i < itemLink.length; i++)
       {
        addGoogleEvent(itemLink[i], 'reading history', 'item');
       }
     }
     checkCookie(rh);
     addClass('lr-about-rh');
     var anchors = document.querySelectorAll('#iii-token-area a');
     var historyForm = document.getElementsByName('PHISTORYFORM')[0];
 if (historyForm) {
      document.getElementById('rh-warning').style.display = '';
     for (var j = 0; j < anchors.length; j++) {
      if (anchors[j].parentNode === historyForm) {
      anchors[j].setAttribute('class', 'lr-button');
     }
     }
      
     }
   }
   else if (a.indexOf('/top') > -1)
   {
     document.getElementById('my-record-blurb').style.display = 'block';
     document.querySelector('#lrRecHome a').setAttribute('class', 'current');
   }
   else if (a.indexOf('/items') > -1)
   {
    if (nav) {
      document.querySelector('#lrChkoutsAct a').setAttribute('class', 'current');
    }
     var rn = 'lr-about-renewals';
     tableStuff('items');
     var rFail = document.getElementById('renewfailmsg');
     if (rFail)
     {
       ga('send', 'event', 'renewal', 'failure');
     } // what is the element that shows on success?
     var b = document.getElementsByTagName('b');
     for (var x = 0; x < b.length; x++) {
      if (b[x].innerHTML.indexOf('RENEWED') > -1) {
        ga('send', 'event', 'renewal', 'success');
        break;
      }
     }
     if (nav) {
      checkCookie(rn);
     }
     addClass('lr-about-renewals');
   }
   else if (a.indexOf('/holds') > -1)
   {
    if (nav) {
     document.querySelector('#lrHoldsAct a').setAttribute('class', 'current');
    }
     var ho = 'lr-about-holds';
     tableStuff('holds');
     checkCookie(ho);
     addClass('lr-about-holds');
   }
   var inputs = document.querySelectorAll('.patFuncArea input[type="submit"], #iii-token-area input[type="submit"]');
   for (var y = 0; y < inputs.length; y++) {
    inputs[y].setAttribute('class', 'lr-button');
    inputs[y].value = inputs[y].value.toLowerCase();
   }
   /*
if (a.indexOf('/top') === -1) {
var blurbs = document.querySelectorAll('.my-record-blurbs');
for (var j = 0; j < blurbs.length; blurbs++) {
blurbs[i].querySelector('button').style.display = 'none';
}

}
*/
 })();

 function getCookie(cname)
 {
   var name = cname + '=';
   var ca = document.cookie.split(';');
   for (var i = 0; i < ca.length; i++)
   {
     var c = ca[i];
     while (c.charAt(0) == ' ') c = c.substring(1);
     if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
   }
   return '';
 }


 function getRecNo(url)
 {
   var recString = url.match(/b.*/);
   recNo = recString[0].replace('~S9', '');
   return recNo;
 }
 /*
 (function ()
 {
   // hide borders if area has no content
   var a = document.querySelectorAll('#main-nav li');
   for (var i = 0; i < a.length; i++)
   {
     var b = a[i];
     if (b.textContent.search(/\w/) === -1)
     {
       b.parentNode.removeChild(b);
     }
   }
 })();\
 */
 (function ()
 {
   var bookLinks = document.querySelectorAll('.patFuncTitle a, .patFuncEntry a');
   if (bookLinks.length > 0)
   {
     var college = getCookie('homeLibrary');
     var colData = [
                    {abbr: 'arc',
                    custid: 'amerriv'},
                    {
                      abbr: 'crc',
                      custid: 'cosum'
                    },
                    {
                    abbr: 'flc',
                    custid: 'ns015092'
                    },
                    {abbr: 'scc',
                    custid: 'sacram'}
                    
                    ];
     var custid;
     for (var j=0; j < colData.length; j++) {
      if (colData[j].abbr === college) {
        custid = colData[j].custid;
      }
     }
     if (college !== '')
     {
       for (var i = 0; i < bookLinks.length; i++)
       {
         var bookLink = bookLinks[i];
         var opacURL = bookLink.getAttribute('href');
         var num = getRecNo(opacURL);
         var ebLink = 'http://search.ebscohost.com/login.aspx?authtype=ip,guest&custid=' + custid + '&groupid=main&profile=eds&direct=true&db=cat01047a&AN=lrois.' + num + '&site=eds-live&scope=site';
         bookLinks[i].setAttribute('href', ebLink);
         bookLinks[i].setAttribute('target', '_top');
       }
     }
   }
 })();
 // just using css for this now
 (function ()
 {
   // move asides to top when screen is small
   var body = document.getElementsByTagName('body')[0];
   if (body.clientWidth < 1101)
   {
     // var asideOne = document.getElementById('patNameAddress');
     var logout = document.getElementById('logout');
     //    logout.setAttribute('class', 'logout-mobile');
     // body.insertBefore(asideOne, body.querySelector('#main'));
     body.insertBefore(logout, body.querySelector('#nav'));
   }
 })();
 (function ()
 {
   // hide menu on click, for mobile
   var a = document.getElementById('nav-label');
   var b = document.getElementById('main-nav');
   if (a)
   {
    if (document.getElementsByTagName('body')[0].offsetWidth < 780) {
      a.style.display = '';
      b.setAttribute('style', 'display:none');
      b.setAttribute('class', 'mobile');
    }
     a.addEventListener('click', function ()
     {
       if (b.getAttribute('style') === null)
       {
         b.setAttribute('style', 'display:none');
       }
       else
       {
         b.removeAttribute('style');
       }
     });
   }
 })();
 (function ()
 {
   // truncate text so long titles don't show
   var a = document.querySelectorAll('.patFuncTitleMain');
   if (a.length > 0)
   {
     for (var i = 0; i < a.length; i++)
     {
       var text = a[i].innerHTML;
       // console.log(text.length);
       if (text.length > 200)
       {
         var shorter = text.substring(0, 200);
         a[i].innerHTML = text.replace(text, shorter + '...');
       }
     }
   }
 })();
 

 function checkCookie(ck)
 {
   var a = getCookie(ck);
   if (a === 'y') {
    var optOutNotice = document.getElementById('ga-intro');
    var gaLink = document.getElementById('ga-opt-out');
    if (optOutNotice) {
      optOutNotice.innerHTML = optOutNotice.innerHTML.replace('on this site', 'on this site. You have opted out of tracking');
    }
    if (gaLink) {
      gaLink.innerHTML = gaLink.innerHTML.replace('&amp; opt out.', '');
    }
   }
 }
 var gaOptOut = document.getElementById('ga-opt-out');
 if (gaOptOut) {
  checkCookie('lrGAOptOut');
  gaOptOut.addEventListener('click', function(e) {
    e.preventDefault();
    var sHeight = screen.availHeight;
    var winHeight = sHeight * 0.7;
    var topOffset = sHeight * 0.15;
    var sWidth = screen.width;
    var winWidth = 700;
    var leftOffset = (sWidth/2) - (winWidth/2);
    var optPage = this.href;
    var optOutWin = window.open(optPage, 'optOut', 'height=' + winHeight + ', width=' + winWidth + ', menubar=no, left='+ leftOffset + ', top=' + topOffset + '');
    optOutWin.focus();
  });

 }
