(function ()
{
  // send email to patron
  var lrPatron = getC('lrUser');
  if (lrPatron !== '')
  {
    var params = getItemValues();
    var itemTitle = params.title;
    var destination = params.dest;
    var status = params.status;
    sendMsg(lrPatron, itemTitle, destination, status);
    if ((destination.indexOf('West Sac') > -1) || (destination.indexOf('Davis') > -1))
    { // for checking on circ to SCC centers
      var callNo = document.querySelector('.bibItemsEntry td:nth-of-type(2)');
      if (callNo)
      {
        var cn = callNo.textContent;
        sendCentersInfo(destination, cn, itemTitle);
      }
    }
  }
})();

function getItemValues()
{
  var suc = document.getElementById('success-message');
  var itemTitle = suc.querySelector('strong').innerHTML;
  var destination = document.getElementById('location').innerHTML;
  var status = getC('itemStatus');
  return {
    title: itemTitle,
    dest: destination,
    status: status
  };
}

function sendMsg(user, item, dest, stat)
{
  var xhr = new XMLHttpRequest();
  var userPatt = /^w\d{6,8}/i;
  if ((userPatt.test(user) === true) && (user.indexOf('@') === -1))
  {
    user = user + '@apps.losrios.edu';
  }
  var fileLoc = 'resources/';
  if (location.port === '444') {
    fileLoc = 'scc/bitbucket/eds-complete/';
  }
  var url = 'https://www.library.losrios.edu/' + fileLoc + 'onesearch/shared/request-send.php';
  console.log(url);
  var params = 'user=' + user + '&item=' + item + '&dest=' + dest + '&status=' + stat;
  xhr.open('POST', url, true); // using post because of length of title... other reasons? Probably could use GET
  //Send the proper header information along with the request
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.onreadystatechange = function ()
  { //Call a function when the state changes.
    if (xhr.readyState == 4 && xhr.status == 200)
    {
      if (xhr.responseText === 'success')
      {
        var confirm = '<p id="email-confirm">An email confirming your request has been sent to <span id="sent-email-address">' + user + '</span></p>';
        var prompt = '<div id="end-prompt"><p><strong>Your request is now complete.</strong></p><p>You may now close this window or <button id="alt-send">send this information to another email address</button>.</p>';
        if (!(document.getElementById('end-prompt'))) {
          confirm += prompt;
        }
        document.getElementById('pickup-message').insertAdjacentHTML('afterend', confirm);
        var addEmail = document.getElementById('add-email');
        if (addEmail.style.display === '') {
          addEmail.appendChild(document.getElementById('email-confirm'));
          addEmail.appendChild(document.getElementById('end-prompt'));
        }
        document.getElementById('alt-send').addEventListener('click', function() {
        afterSend(item, dest, stat, false);
        });
      }
      else
      {
        console.log('send error');
        afterSend(item, dest, stat, true);
      }
    }
  };
  xhr.send(params);
}

function afterSend(item, dest, stat, er)
{
  var div = document.getElementById('add-email');
  if (er === true)
  {
    div.innerHTML = div.innerHTML.replace('a different', 'an');
  }
  div.style.display = '';
  var emailInput = document.getElementById('alt-user');
  emailInput.value = '';
  emailInput.focus();
  var sendAnother = document.getElementById('send-another');
  sendAnother.removeEventListener('submit', sendAltEmail);
  sendAnother.addEventListener('submit', sendAltEmail);
}

function sendAltEmail()
{
  var emailInput = document.getElementById('alt-user');
  var user = emailInput.value;
  // need to check for valid email address
  var emailPatt = /^.+@[^\.].*\.[a-zA-Z]{2,30}$/;
  var oldErr = document.getElementById('send-another-error');
  if (emailPatt.test(user) === false)
  {
    if (oldErr)
    {
      oldErr.parentNode.removeChild(oldErr);
    }
    var err = document.createElement('div');
    err.id = 'send-another-error';
    err.innerHTML = user + ' is not a valid email address. Please enter a valid email address.';
    err.setAttribute('role', 'alert');
    err.setAttribute('tabindex', '0');
    err.setAttribute('class', 'error');
    document.getElementById('send-another').appendChild(err);
    document.getElementById('send-another-error').focus();
    emailInput.focus();
  }
  else
  {
    if (oldErr)
    {
      oldErr.parentNode.removeChild(oldErr);
    }
    var confirm = document.getElementById('email-confirm');
    if (confirm)
    {
      confirm.parentNode.removeChild(confirm);
    }
    var params = getItemValues();
    var item = params.title;
    var dest = params.dest;
    var stat = params.status;
    sendMsg(user, item, dest, stat);
  }
}

function sendCentersInfo(loc, callNo, t)
{
  var xhttp = new XMLHttpRequest();
  xhttp.open('POST', 'https://wserver.scc.losrios.edu/~library/tools/centers/notify.php', true);
  xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhttp.send('location=' + loc + '&cn=' + callNo + '&title=' + t);
}