console.log('port is ' +location.port);
(function() { // remove start over button and change background color in iframes, for EDS
  if (location.hash === '#onesearch') {
    document.cookie = 'oneSearchReq=y;path=/;domain=.losrios.edu';
  }
  var osr = getC('oneSearchReq');
  if ((window.location !== window.parent.location) || (osr === 'y') || (location.hash === 'onesearch')) {

var a = document.createElement('link');
a.setAttribute('type', 'text/css');
a.setAttribute('href', '/screens/iframe-styles.css');
a.setAttribute('rel', 'stylesheet');
var b = document.getElementsByTagName('head')[0];
b.appendChild(a);
  }
})();
function getC(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
} 
function showHelp(e)
{
  var el = document.getElementById(e);
  var elClass = el.getAttribute('class');
  if (elClass.indexOf('hidden') > -1)
  {
    // first check if any are unhidden and hide any that are
    var helps = document.querySelectorAll('#login-fields .ua-help');
    for (var i = 0; i < helps.length; i++)
    {
      var help = helps[i];
      if (help.getAttribute('class').indexOf('hidden') === -1)
      {
        console.log('found');
        help.className += ' hidden';
      }
    }
    // then strip hidden from this ell
    var newClass = elClass.replace(' hidden', '');
    el.setAttribute('class', newClass);
    el.querySelector('h2').setAttribute('aria-hidden', 'false');
  }
}

function locHelp(el, loc)
{
  el.addEventListener('click', function ()
  {
    //    var loc = this.value;
    var stateLoc;
    switch (loc)
    {
    case 'ac ':
      stateLoc = 'ARC Library Circulation Desk';
      break;
    case 'cc ':
      stateLoc = 'Cosumnes River College Library Circulation Desk';
      break;
    case 'ec ':
      stateLoc = 'El Dorado Center Library Circulation Counter';
      break;
    case 'fc ':
      stateLoc = 'FLC Library Circulation Counter';
      break;
    case 'sc ':
      stateLoc = 'Sacramento  City College Library Circulation Desk';
      break;
    case 'acnat':
      stateLoc = 'North Natomas Public Library Circulation Desk (adjacent to ARC Natomas Center)';
      break;
    case 'ccbkt':
      stateLoc = 'CRC Elk Grove Center Learning Resource Center (EGA 204)';
      break;
    case 'sodav':
      stateLoc = 'SCC Davis Center Learning Resource Center (Room 215)';
      break;
    case 'sowsa':
      stateLoc = 'SCC West Sacramento Center Learning Resource Center (3rd Floor)';
      break;
    default:
      stateLoc = '';
    }
    var statement = '<p>Item will be held at the ' + stateLoc + '.</p>';
    var locHelpArea = document.getElementById('loc-help');
    locHelpArea.innerHTML = statement;
	if (className.indexOf('highlighted') === -1) {
		locHelpArea.className += ' highlighted';
	}
  });
}
(function ()
{
  var radios = document.querySelectorAll('#loc-fields input');
  for (var i = 0; i < radios.length; i++)
  {
    var a = radios[i];
    var b = a.value;
    locHelp(a, b);
  }
})();


(function(){ // take question mark help buttons out of tab flow - they will now come after the password input
if (document.getElementById('lr-sign-in')) { // this should only run on initial sign-in screen
var tabindex = 1;
var a = document.querySelectorAll('input');
for (var i = 0; i < a.length; i++) {
	if (a[i].getAttribute('class').indexOf('show-help') === -1) {
		a[i].setAttribute('tabindex', tabindex);
		tabindex+= 10;
	}
	}
}

})();

(function() { // improve display of request feature on mobile devices
 if (screen.width < 800) {
  var a = document.createElement('style');
    a.setAttribute('type', 'text/css');
    a.innerHTML = 'body {background:#fff;} #OuterTable {width:100%; border:none;} #Header1, #Header2, #Footer, #start-over {display:none;} .selectable:hover, .selectable:focus {background:#ddd !important; } .selected td {font-weight:bold;} .bibItemsEntry td {cursor:default;}';
    var head = document.getElementsByTagName('head')[0];
    head.appendChild(a);
    var b = document.createElement('meta');
    b.setAttribute('name', 'viewport');
    b.setAttribute('content', 'width=device-width', 'inital-scale=1');
    head.appendChild(b);
}
})();
(function() {
var rows = document.querySelectorAll('.bibItemsEntry');
if (rows.length > 1) {
 setSelectable();

for (var i = 0; i < rows.length; i++) {
  var a = rows[i];
  if (a.querySelector('td:first-of-type input')) {
    a.addEventListener('click', setRadio);
  }
}
}
})();
function setRadio() {
  var radio = this.querySelector('td:first-of-type input');
  if (radio) {
    radio.checked = true;
    setSelectable();
  }
}

function setSelectable() {
  var rows = document.querySelectorAll('.bibItemsEntry');


 
  for (var i = 0; i < rows.length; i++) {
    var a = rows[i];
    var b = a.querySelector('td:first-of-type input');
    if (b) {
      if (b.checked === true) {
	a.setAttribute('class', 'bibItemsEntry selected');
      }
      else {
	a.setAttribute('class', 'bibItemsEntry selectable');
      }
    }

}
 
}