# README #

This is the contents of our live OPAC Screens directory. Much of it is legacy "LOIS" stuff. However, there have been lots of changes to the login and patron record screens.

Note that these pages pull in the stylesheet used by the databases page. This is done so that we don't have to make changes to that stylesheet in two different places. That stylesheet is in the [databases page repository](https://bitbucket.org/los-rios-libraries/databases-page) (style.css).