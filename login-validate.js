// check for obvious login problems
(function ()
{
  var signInForm = document.getElementById('lr-sign-in');
  signInForm.addEventListener('submit', function (e)
  {
    e.preventDefault();
    var wid = document.getElementById('extpatid');
    widVal = wid.value;
    var pw = document.getElementById('extpatpw');
    pwVal = pw.value;
    var idHolder = document.getElementById('id-container');
    var pwHolder = document.getElementById('pw-container');
    var lrError = document.querySelector('.error');
    var cat = 'login error';
    var locFields = document.getElementById('loc-fields');
    if (lrError)
    {
      lrError.parentNode.removeChild(lrError);
    }
    if (widVal === '')
    {
      addPrompt(idHolder, 'Please enter your username', wid);
      if (typeof showTips === 'function')
      {
        showTips('un-help');
      }
      if (typeof ga === 'function')
      {
        ga('send', 'event', cat, 'display', 'username missing');
      }
    }
    else if (/(w\d{6,8})|libguest/i.test(widVal) === false)
    {
      // console.log('bad pattern');
      addPrompt(idHolder, 'Username entered incorrectly. Enter a W followed by your student ID number.', wid);
      if (typeof ga === 'function')
      {
        ga('send', 'event', cat, 'display', 'username bad');
      }
      if (typeof showTips === 'function')
      {
        showTips('un-help');
      }
    }
    else if (pwVal === '')
    {
      addPrompt(pwHolder, 'Please enter a password.', pw);
      if (typeof ga === 'function')
      {
        ga('send', 'event', cat, 'display', 'password missing');
      }
      if (typeof showTips === 'function')
      {
        showTips('pw-help');
      }
    }
    else if (pwVal.search(/[A-Z]/) === -1)
    {
      addPrompt(pwHolder, 'Your password must contain at least one capital letter. Did you mistype it?', pw);
      if (typeof ga === 'function')
      {
        ga('send', 'event', cat, 'display', 'password no caps ');
      }
    }
    else if (pwVal.search(/[a-z]/) === -1)
    {
      addPrompt(pwHolder, 'Your password must contain at least one lower-case letter. Did you mistype it?', pw);
      if (typeof ga === 'function')
      {
        ga('send', 'event', cat, 'display', 'password no lc');
      }
      if (typeof showTips === 'function')
      {
        showTips('pw-help');
      }
    }
    else if (pwVal.search(/[\d\!"#\$%&'\(\)\*\+,\-\.\/:;<=>\?@\[\\\]\^_`\{\|\}~]/) === -1)
    { // apparently district allows special characters instead of numbers
      addPrompt(pwHolder, 'Your password must contain at least one number. Did you mistype it?', pw);
      if (typeof ga === 'function')
      {
        ga('send', 'event', cat, 'display', 'password no nums ');
      }
      if (typeof showTips === 'function')
      {
        showTips('pw-help');
      }
    }
    else if (pwVal.length < 10)
    {
      addPrompt(pwHolder, 'Your password must contain at least 10 characters. Did you mistype it?', pw);
      if (typeof ga === 'function')
      {
        ga('send', 'event', cat, 'display', 'password too short');
      }
      if (typeof showTips === 'function')
      {
        showTips('pw-help');
      }
    }
    else if (locFields)
    {
      /*  console.log('yes, locFields');
      var radios = document.querySelectorAll('input[type="radio"]');
      for (var i = 0; i < radios.length; i++)
      {
        if (radios[i].checked)
        {
break;
        }
        else
        {
          addPrompt(locFields, 'You must select a pickup location.', locFields);
          break;
        }
      }
      */
      if (!(document.querySelector('input[name="loc"]:checked')))
      {
        addPrompt(locFields, 'You must select a pickup location.', locFields);
      }
      else
      {
        //     signInForm.submit();
        submitForm(this, widVal);
      }
    }
    else
    {
      submitForm(this, widVal);
    }
  });

  function submitForm(e, uid)
  {
    if (uid === 'libguest')
    {
      var c = confirm('You are signing in under the Los Rios guest access account. This account is intended only for vendors or others performing work on behalf of the Los Rios libraries. It is not to be meant by users unaffiliated with the Los Rios Libraries. If you do not fall into this category, you should not proceed.\n\nDo you agree?');
      if (c === true)
      {
        e.submit();
      }
    }
    else
    {
      e.submit();
    }
  }

  function addPrompt(el, message, f)
  {
    var errorMsg = document.createElement('div');
    errorMsg.setAttribute('class', 'error');
    errorMsg.innerHTML = '<div class="errorMsg" role="alert">' + message + '</div>';
    el.appendChild(errorMsg);
    if (f === document.getElementById('loc-fields'))
    {
      f.focus();
    }
    else
    {
      f.select();
    }
  }
})();
(function ()
{
  document.getElementById('extpatid').focus(); // focus on load
  var b = document.getElementById('error');
  if (b)
  {
    ga('send', 'event', 'login error', 'display', 'server-side');
  }
  var c = document.querySelectorAll('#main a');
  for (var j = 0; j < c.length; j++)
  {
    var link = c[j];
    var text = link.innerHTML;
    lrLinkListener(link, text);
  }
})();

function lrLinkListener(link, text)
{
  link.addEventListener('click', function ()
  {
    if (ga)
    {
      ga('send', 'event', 'help links', 'click', text);
    }
  });
}